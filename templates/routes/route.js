'use strict';

var <%= modelNameUCase %> = require('../models/<%= modelNameLCase%>');

/**
 * @apiDefine UncaughtException
 *
 * @apiError (Error 5xx) UncaughtException Uncaught Exception
 *
 * @apiErrorExample {json} Uncaught Exception
 *     HTTP/1.1 500 Not Found
 *     {
 *        "error": {Error}
 *     }
 */

module.exports = function(router) {

  router.route('/<%= modelPluralLCase %>')
    /**
     * @api {get} /<%= modelPluralLCase %> Get <%= modelPluralLCase %>
     * @apiVersion 0.1.0
     * @apiGroup <%= modelName %>
     * @apiDescription Get a listing of <%= modelPluralLCase %>
     *
     * @apiPermission Authentication Required
     *
     * @apiSuccess {Array} <%= modelPluralLCase %>                                      Array of all <%= modelPluralLCase %>
     * @apiSuccess {Object} <%= modelPluralLCase %>.<%= modelNameLCase %>               Object containing <%= modelName %> details
     * @apiSuccess {ObjectId} <%= modelPluralLCase %>.<%= modelNameLCase %>._id         <%= modelName %> Object Id
     * @apiSuccess {String} <%= modelPluralLCase %>.<%= modelNameLCase %>.name          <%= modelName %> Name
     * @apiSuccess {String} <%= modelPluralLCase %>.<%= modelNameLCase %>.createdAt     Timestamp of when the <%= modelName %> created
     * @apiSuccess {String} <%= modelPluralLCase %>.<%= modelNameLCase %>.updatedAt     Timestamp of when the <%= modelName %> was last updated
     *
     * @apiSuccessExample {json} Success-Response
     *     HTTP/1.1 200 OK
     *     [
     *       {
     *         "_id": "507f191e810c19729de860ea",
     *         "name": "<%= modelName %> ABC"
     *        }
     *      ]
     *
     * @apiUse UncaughtException
     */
    .get(function(req, res) {
      <%= modelNameUCase %>.find({deleted: false}, function(err, <%= modelPluralLCase %>) {
        if (err) throw new Error(err);

        res.status(200).json(<%= modelPluralLCase %>);
      });
    })

    /**
     * @api {post} /<%= modelPluralLCase %> Add a <%= modelName %>
     * @apiVersion 0.1.0
     * @apiGroup <%= modelName %>
     * @apiDescription Add a new <%= modelName %>
     *
     * @apiPermission Authentication Required
     *
     * @apiParam {String} name                        <%= modelName %> Name
     *
     * @apiSuccess {String} message                               <%= modelName %> Added Successfully
     * @apiSuccess {Object} <%= modelNameLCase %>                 Added <%= modelName %> Object
     * @apiSuccess {Object} <%= modelNameLCase %>._id             <%= modelName %> Object Id
     * @apiSuccess {Object} <%= modelNameLCase %>.name            <%= modelName %> Name
     * @apiSuccess {String} <%= modelNameLCase %>.createdAt       Timestamp of when the <%= modelName %> was created
     * @apiSuccess {String} <%= modelNameLCase %>.updatedAt       Timestamp of when the <%= modelName %> last updated
     *
     * @apiSuccessExample {json} Success-Response
     *     HTTP/1.1 200 OK
     *     {
     *        "message": "<%= modelName %> Added Successfully",
     *        "<%= modelNameLCase %>": {
     *          "_id": "507f191e810c19729de860ea",
     *          "name": "<%= modelName %> ABC",
     *          "createdAt": "2020-04-10T03:22:26.068Z",
     *          "updatedAt": "2020-04-10T03:22:26.068Z"
     *        }
     *     }
     *
     * @apiError <%= modelName %>AlreadyExists <%= modelName %> with that <code>name</code> already exists
     *
     * @apiErrorExample {json} <%= modelName %> Already Exists
     *     HTTP/1.1 409 Already Exists
     *     {
     *        "message": "<%= modelName %> Already Exists"
     *     }
     *
     * @apiUse UncaughtException
     */
    .post(function(req, res) {
      var new<%= modelNameUCase %> = new <%= modelNameUCase%>();

      // Add Fields here
      new<%= modelNameUCase %>.name = req.body.name;
      // new<%= modelNameUCase %>. = req.body.

      <%= modelNameUCase%>.findOne({name: new<%= modelNameUCase%>.name}, function(err, <%= modelNameLCase %>){
        if (err) throw new Error(err);

        if (<%= modelNameLCase %>) {
          res.status(409).json({message: '<%= modelNameUCase%> Already Exists'});
        } else {
          new<%= modelNameUCase%>.save(function(err) {

            if (err) throw new Error(err);

            var clean<%= modelNameUCase%> = new<%= modelNameUCase%>.toObject();
            delete clean<%= modelNameUCase%>['deleted'];

            res.status(200).json(clean<%= modelNameUCase%>);
          });
        }
      });
    })
  ;

  router.route('/<%= modelPluralLCase %>/:id')
    /**
     * @api {get} /<%= modelPluralLCase %>/:id Get a <%= modelName %>
     * @apiVersion 0.1.0
     * @apiGroup <%= modelName %>
     * @apiDescription Get a specific <%= modelName %>
     *
     * @apiPermission Authentication Required
     *
     * @apiParam {String} id  ID of <%= modelName %>
     *
     * @apiSuccess {Object} <%= modelNameLCase %>                <%= modelName %> Object
     * @apiSuccess {ObjectId} <%= modelNameLCase %>._id          <%= modelName %> Object Id
     * @apiSuccess {String} <%= modelNameLCase %>.name           <%= modelName %> Name
     * @apiSuccess {String} <%= modelNameLCase %>.createdAt      Timestamp of when the <%= modelName %> was created
     * @apiSuccess {String} <%= modelNameLCase %>.updatedAt      Timestamp of when the <%= modelName %> was last updated
     *
     * @apiSuccessExample {json} Success-Response
     *     HTTP/1.1 200 OK
     *     {
     *            "_id": "507f191e810c19729de860ea",
     *            "name": "<%= modelName %> ABC",
     *            "createdAt": "2020-04-10T03:22:26.068Z",
     *            "updatedAt": "2020-04-10T03:22:26.068Z"
     *     }
     *
     * @apiError <%= modelName %>DoesNotExist <%= modelName %> with that <code>ID</code> does not exists
     *
     * @apiErrorExample {json} <%= modelName %> Does Not Exist
     *     HTTP/1.1 404 Does Not Exist
     *     {
     *        "message": "<%= modelName %> does not exist"
     *     }
     *
     * @apiUse UncaughtException
     */
    .get(function(req, res) {
      <%= modelNameUCase %>.findById(req.params.id)
        .exec(function(err, <%= modelNameLCase %>) {
          if (err) throw new Error(err);

          if (<%= modelNameLCase %>) {
            res.status(200).json(<%= modelNameLCase %>);
          } else {
            return res.status(404).json({message: '<%= modelNameUCase %> does not exist'});
          }
        });
    })

    /**
     * @api {put} /<%= modelPluralLCase %>/:id Update a <%= modelName %>
     * @apiVersion 0.1.0
     * @apiGroup <%= modelName %>
     * @apiDescription Update a specific <%= modelName %>
     *
     * @apiPermission Authentication Required
     *
     * @apiParam {String} name                  <%= modelName %> Name
     *
     * @apiSuccess {String} message                               <%= modelName %> Updated Successfully
     * @apiSuccess {Object} <%= modelNameLCase %>                 Added <%= modelName %> Object
     * @apiSuccess {ObjectId} <%= modelNameLCase %>._id           Database ID of the <%= modelName %>
     * @apiSuccess {String} <%= modelNameLCase %>.name            <%= modelName %> Name
     * @apiSuccess {String} <%= modelNameLCase %>.createdAt       Timestamp of when the <%= modelName %> was created
     * @apiSuccess {String} <%= modelNameLCase %>.updatedAt       Timestamp of when the <%= modelName %> was last updated
     *
     *
     * @apiSuccessExample {json} Success-Response
     *     HTTP/1.1 200 OK
     *     {
     *         message: "<%= modelName %> Updated Successfully",
     *         <%= modelNameLCase %>: {
     *           "_id": "507f191e810c19729de860ea",
     *           "name": "<%= modelName %> ABC",
     *           "createdAt": "2020-04-10T03:22:26.068Z",
     *           "updatedAt": "2020-04-10T03:22:26.068Z"
     *         }
     *     }
     *
     * @apiError <%= modelName %>AlreadyExists <%= modelName %> with that <code>name</code> already exists<br>
     * @apiError <%= modelName %>DoesNotExist <%= modelName %> with that <code>ID</code> does not exists
     *
     * @apiErrorExample {json} <%= modelName %> Name Alread Exists
     *     HTTP/1.1 409 Already Exists
     *     {
     *        "message": "<%= modelName %> Name Already Exists"
     *     }
     *
     * @apiErrorExample {json} <%= modelName %> Does Not Exist
     *     HTTP/1.1 404 Does Not Exist
     *     {
     *        "message": "<%= modelName %> ID Does Not Exist"
     *     }
     *
     * @apiUse UncaughtException
     */
    .put(function(req, res) {
      <%= modelNameUCase %>.findById(req.params.id, function(err, <%= modelNameLCase %>) {
        if (<%= modelNameLCase %>) {
          if (err) throw new Error(err);

          /* Add your field updates here */
          <%= modelNameLCase %>.name = req.body.name;
          <%= modelNameLCase %>.active = req.body.active;

          <%= modelNameLCase %>.save(function(err) {
            if (err) throw new Error(err);

            res.status(200).json(<%= modelNameLCase %>);
          });
        } else {
          res.status(409).json({message: '<%= modelNameUCase %> ID Does Not Exist'});
        }
      });
    })

    /**
     * @api {delete} /<%= modelPluralLCase %>/:id Delete a <%= modelName %>
     * @apiVersion 0.1.0
     * @apiGroup <%= modelName %>
     * @apiDescription Delete a specific <%= modelName %>
     *
     * @apiPermission Authentication Required
     *
     * @apiParam {String} id  ID of <%= modelName %>
     *
     * @apiSuccess {String} message     <%= modelName %> Deleted
     *
     * @apiSuccessExample {json} Success-Response
     *     HTTP/1.1 200 OK
     *     {
     *          "message": "<%= modelName %> Deleted"
     *     }
     *
     * @apiError <%= modelName %>DoesNotExist <%= modelName %> does not exist
     *
     * @apiErrorExample {json} <%= modelName %> Does Not Exist
     *     HTTP/1.1 404 Does Not Exist
     *     {
     *        "message": "<%= modelName %> does not exist"
     *     }
     *
     * @apiUse UncaughtException
     */
    .delete(function(req, res) {
      <%= modelNameUCase %>.findById(req.params.id, function(err, <%= modelNameLCase %>) {
        if (err) throw new Error(err);

        if (<%= modelNameLCase %>) {
          <%= modelNameLCase %>.deleted = true;
          <%= modelNameLCase %>.save(function(err) {
            if (err) throw new Error(err);

            res.status(200).json({message: '<%= modelNameUCase %> Deleted'});
          });
        } else {
          return res.status(400).json({message: '<%= modelNameUCase %> does not exist'});
        }
      });
    })
  ;
};
