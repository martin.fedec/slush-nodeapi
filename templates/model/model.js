'use strict';

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var <%= modelNameUCase %>Schema = new Schema({
  name: String,
  active: { type: Boolean, default: true },
  deleted: { type: Boolean, select: false, default: false },
}, {timestamps: true});

module.exports = mongoose.model('<%= modelNameUCase %>', <%= modelNameUCase %>Schema);
